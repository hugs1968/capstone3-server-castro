const User = require("../models/User")
const UserController = require("../controllers/user")
const express = require('express')
const router = express.Router()
const auth = require("../auth")





router.get("/", (req, res) => {

	UserController.getAllUsers().then(resultFromGetAllUsers => res.send(resultFromGetAllUsers))

})

router.get('/details', auth.verify, (req, res) => {
	
	const user = auth.decode(req.headers.authorization)

	UserController.get({userId: user.id}).then(user => res.send(user))
	
})

router.get('/categories', auth.verify, (req, res) => {

	UserController.getAllCategories().then(resultFromGetAllCategories => res.send(resultFromGetAllCategories))

})


router.post('/', (req, res) => {

	UserController.register(req.body).then(resultFromRegister => res.send(resultFromRegister))
	
})

router.post('/email-exists', (req,res)=>{
	UserController.emailExists(req.body).then(resultFromEmailExists => res.send(resultFromEmailExists))

})

router.post('/login', (req, res)=>{

	UserController.login(req.body).then(resultFromLogin => res.send(resultFromLogin))

})


module.exports = router
