const User = require("../models/User")

const bcrypt = require("bcrypt")

const auth = require("../auth")



module.exports.emailExists = (parameterFromRoute) => {

	return User.find({ email: parameterFromRoute.email }).then(resultFromFind => {

		return resultFromFind.length > 0 ? true : false

	})
}


module.exports.register = (params) => {

	console.log(params)

	let newUser = new User({

		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,		
		password: bcrypt.hashSync(params.password, 10)

	})

	return newUser.save().then((user, err)=>{

		return (err) ? false : true

	})

	console.log(user)

}


module.exports.login = (params) => {

	return User.findOne({email: params.email}).then(resultFromFindOne => {

		if(resultFromFindOne == null) {

			return false

		}

		const isPasswordMatched = bcrypt.compareSync(params.password, resultFromFindOne.password)

		console.log(isPasswordMatched)

		if(isPasswordMatched){

			return { accessToken: auth.createAccessToken(resultFromFindOne)}

		} else {

			return false
		}



	})

}


module.exports.get = (params) => {

	console.log(params)

	return User.findById(params.userId).then(resultFromFindById => { 

		resultFromFindById.password = undefined

		return resultFromFindById
	
	})

}


module.exports.categories = (params) => {

	console.log(params)

}